from django.contrib import admin
from django.contrib.admin.utils import flatten_fieldsets

from uke_base.actions import restore
from uke_base.filters import NullFilterSpec
from uke_base.models import SoftDeletionModel


class ReadOnlyInline(admin.TabularInline):
    show_change_link = True
    can_delete = False
    extra = 0

    def has_add_permission(self, request):
        return False


class FechaMixin:
    def format_fecha(self, obj):
        return obj.fecha.strftime('%m/%Y')

    format_fecha.admin_order_field = 'fecha'
    format_fecha.short_description = 'Fecha'

    def format_fecha_vencimiento(self, obj):
        return obj.fecha.strftime('%d %b')

    format_fecha_vencimiento.admin_order_field = 'fecha_vencimiento'
    format_fecha_vencimiento.short_description = 'Vto'


class SoftDeleteAdminMixin(admin.ModelAdmin):

    def get_list_filter(self, request):
        if not issubclass(self.model, SoftDeletionModel):
            return super(SoftDeleteAdminMixin, self).get_list_filter(request)

        return super(SoftDeleteAdminMixin, self).get_list_filter(request) + (
            ('deleted_at', NullFilterSpec),
        )

    def get_actions(self, request):

        if not issubclass(self.model, SoftDeletionModel):
            return super(SoftDeleteAdminMixin, self).get_actions(request)

        if not request.GET.has_key('deleted_at__isnull'):
            return super(SoftDeleteAdminMixin, self).get_actions(request)

        from collections import OrderedDict
        res = super(SoftDeleteAdminMixin, self).get_actions(request)

        d = OrderedDict(res)
        (func, name, desc) = self.get_action(restore)
        d[name] = (func, name, desc)

        return d

    def get_queryset(self, request):
        """
                Returns a QuerySet of all model instances that can be edited by the
                admin site. This is used by changelist_view.
                """
        if not issubclass(self.model, SoftDeletionModel):
            return super(SoftDeleteAdminMixin, self).get_queryset(request)

        if not request.GET.has_key('deleted_at__isnull'):
            return super(SoftDeleteAdminMixin, self).get_queryset(request)

        qs = self.model.all_objects.get_queryset()

        # TODO: this should be handled by some parameter to the ChangeList.
        ordering = self.get_ordering(request)
        if ordering:
            qs = qs.order_by(*ordering)
        return qs


class ReadOnlyAdmin(admin.ModelAdmin):
    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]
        # if request.user.is_superuser:
        #     return self.readonly_fields

        if hasattr(self, 'declared_fieldsets'):
            return flatten_fieldsets(self.declared_fieldsets)
        else:
            return list(set(
                [field.name for field in self.opts.local_fields] +
                [field.name for field in self.opts.local_many_to_many]
            ))
