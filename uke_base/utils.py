import datetime

from django.utils import timezone


def one_year_from_now():
    return timezone.now() + datetime.timedelta(days=365)