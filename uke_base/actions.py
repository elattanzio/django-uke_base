from django.utils.translation import ugettext_lazy

import uke_base.models


def habilitar(modeladmin, request, queryset):
    if issubclass(modeladmin.model, uke_base.models.ActivableModel):
        queryset.update(activo=True)


habilitar.short_description = ugettext_lazy("activar")


def deshabilitar(modeladmin, request, queryset):
    if issubclass(modeladmin.model, uke_base.models.ActivableModel):
        queryset.update(activo=False)


deshabilitar.short_description = ugettext_lazy("desactivar")


def restore(modeladmin, request, queryset):
    if issubclass(modeladmin.model, uke_base.models.SoftDeletionModel):
        queryset.update(deleted_at=None)


restore.short_description = ugettext_lazy("restore model")
