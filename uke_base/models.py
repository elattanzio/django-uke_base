from time import timezone

from django.db import models
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible

from uke_base.managers import SoftDeletionManager, ActibableManager

__all__ = [
    'SoftDeletionModel',
    'TipoDocuMixin',
    'DescriptibleModel',
    'CodificableModel',
    'FechaAltaModel',
    'DomicilioMixin',
    'PersonaMixin',
    'ImporteModel',
    'FechaBajaModel',
    'ActivableModel'
]


class SoftDeletionModel(models.Model):
    deleted_at = models.DateTimeField(blank=True, null=True)

    objects = SoftDeletionManager()
    all_objects = SoftDeletionManager(alive_only=False)

    class Meta:
        abstract = True

    def delete(self):
        if self.deleted_at:
            return super(SoftDeletionModel, self).delete()
        else:
            self.deleted_at = timezone.now()
            self.save()

    def hard_delete(self):
        super(SoftDeletionModel, self).delete()


@python_2_unicode_compatible
class DescriptibleModel(models.Model):
    descripcion = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.descripcion if self.descripcion else ''

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if isinstance(self, CodificableModel) and not self.descripcion:
            self.descripcion = self.codigo

        super(DescriptibleModel, self).save(force_insert, force_update, using, update_fields)

    class Meta:
        abstract = True


class ItemVigenciaModel(models.Model):
    fecha_desde = models.DateField(null=True, blank=True)
    fecha_hasta = models.DateField(null=True, blank=True)

    class Meta:
        abstract = True


@python_2_unicode_compatible
class TipoDocuMixin(models.Model):
    CUIT = '80'
    CUIL = '86'
    DNI = '96'
    TIPOS_DOC_ID = (
        (CUIT, 'CUIT'),
        (CUIL, 'CUIL'),
        (DNI, 'DNI'),
        ('89', 'LE'),
        ('90', 'LC'),
    )
    doc_tipo = models.CharField(max_length=2, null=True, blank=True, choices=TIPOS_DOC_ID, default=DNI)
    doc_nro = models.CharField(max_length=11, null=True, blank=True)
    doc_nro.isnull_filter = True

    @property
    def doc_nro_display(self):
        if self.doc_nro:
            return self.get_doc_tipo_display() + ' ' + self.doc_nro
        return ''

    def __str__(self):
        return self.doc_nro_display

    class Meta:
        abstract = True


class CodificableModel(models.Model):
    codigo = models.SlugField(unique=True, blank=True, null=True)

    class Meta:
        abstract = True


class FechaAltaModel(models.Model):
    fecha = models.DateTimeField(default=timezone.now, null=True, blank=True, verbose_name='fecha alta')

    @property
    def created_at(self):
        return self.fecha

    class Meta:
        abstract = True


class DomicilioMixin(models.Model):
    emails = models.CharField(max_length=255, null=True, blank=True)
    domicilio = models.CharField(max_length=255, null=True, blank=True)
    localidad = models.CharField(max_length=144, null=True, blank=True)
    zip_code = models.CharField(max_length=10, null=True, blank=True)
    provincia_id = models.CharField(max_length=32, null=True, blank=True)
    telefono = models.CharField(max_length=255, null=True, blank=True)

    class Meta:
        abstract = True


@python_2_unicode_compatible
class PersonaMixin(models.Model):
    nombre = models.CharField(max_length=144, null=True, blank=True)
    apellido = models.CharField(max_length=144, null=True, blank=True)

    def __str__(self):
        if self.apellido:
            return self.nombre + ' ' + self.apellido
        else:
            return self.nombre

    class Meta:
        abstract = True


class ImporteModel(models.Model):
    importe = models.DecimalField(max_digits=10, decimal_places=2)

    class Meta:
        abstract = True


class FechaBajaModel(models.Model):
    fecha_baja = models.DateTimeField(null=True, blank=True)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if isinstance(self, ActivableModel):
            self.fecha_baja = None if self.activo else timezone.now()

        super(FechaBajaModel, self).save(force_insert, force_update, using, update_fields)

    class Meta:
        abstract = True


class ActivableModel(models.Model):
    activo = models.BooleanField(default=True)
    objects = ActibableManager()

    class Meta:
        abstract = True
