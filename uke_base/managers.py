from django.db import models
from django.db.models import Sum, Case, When, Value, DecimalField
from django.db.models.functions import Coalesce

from uke_base.querysets import SoftDeletionQuerySet

__all__ = [
    'SoftDeletionManager',
    'ActibableManager',
    'SaldoManager'
]


class SoftDeletionManager(models.Manager):
    def __init__(self, *args, **kwargs):
        self.alive_only = kwargs.pop('alive_only', True)
        super(SoftDeletionManager, self).__init__(*args, **kwargs)

    def get_queryset(self):
        if self.alive_only:
            return SoftDeletionQuerySet(self.model).filter(deleted_at=None)
        return SoftDeletionQuerySet(self.model)

    def hard_delete(self):
        return self.get_queryset().hard_delete()


class ActibableManager(models.Manager):
    def activos(self):
        return self.filter(activo=True)


class SaldoManager(models.Manager):
    TOTAL_Q = Coalesce(Sum(Case(When(
        # agrega los cargos solo de la fecha igual o menor que la liquidacion (no los nuevos)
        cargos__tipo__totalizable=True,
        then='cargos__importe'),
        default_case=Value(0),
        output_field=DecimalField())), 0)

    SALDO_Q = Coalesce(Sum('cargos__importe'), 0)

    def get_queryset(self):
        return super(SaldoManager, self) \
            .get_queryset() \
            .annotate(saldo=self.SALDO_Q,
                      total=self.TOTAL_Q)
