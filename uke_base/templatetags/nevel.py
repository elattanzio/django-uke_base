# Here, register is a django.template.Library instance, as before
import json

from django import template
from django.conf import settings
from django.contrib.auth.models import Group
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models.query import QuerySet

register = template.Library()


@register.filter(name='app_is_installed')
def app_is_installed(value):
    return value in settings.INSTALLED_APPS


@register.filter(name='has_group')
def has_group(user, group_name):
    try:
        group = Group.objects.get(name=group_name)
        return group in user.groups.all()
    except Group.DoesNotExist:
        return False


@register.inclusion_tag('templatetags/pretty_json.html')
def pretty_json(obj):
    try:

        return dict(
            json=json.dumps(obj, indent=4, cls=DjangoJSONEncoder)
        )
    except Exception as ex:
        pass


@register.inclusion_tag('templatetags/model_action.html')
def model_action(model, action, text=None):
    items = model if isinstance(model, list) or isinstance(model, QuerySet) else [model]
    try:
        meta = items[0]._meta if len(items) else None

        actions = [action] if not isinstance(action, list) else action

        if text is None:
            text = action

        def _action(action):
            return {
                'name': action.__name__ if hasattr(action, '__name__') else action,
                'label': action.short_description if hasattr(action, 'short_description') else action,
            }

        return dict(
            chagelist_url='admin:' + meta.app_label + '_' + meta.object_name.lower() + '_changelist',
            items=items,
            actions=map(_action, actions),
            text=text
        )
    except:
        pass
